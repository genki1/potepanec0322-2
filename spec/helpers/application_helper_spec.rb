require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_title function" do
    it "default title" do
      expect(full_title("")).to eq("BIGBAG store")
    end

    it "title include product name" do
      expect(full_title("product name")).to eq("product name - BIGBAG store")
    end

    it "no argument" do
      expect(full_title(nil)).to eq("BIGBAG store")
    end
  end
end
