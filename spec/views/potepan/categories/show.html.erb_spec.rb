require 'rails_helper'

RSpec.describe "categories/show.html.erb", type: :view do
  before do
    product.images << image
    visit potepan_category_path(taxon.id)
  end

  let(:taxon) { create(:taxon, parent: taxonomy.root) }
  let(:taxonomy) { create(:taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:image) { create(:image) }

  describe "user visit category page" do
    scenario "get page title" do
      expect(page).to have_title "#{taxon.name} - BIGBAG store"
    end

    scenario "get taxon info" do
      expect(page).to have_content taxon.name
    end

    scenario "get product info" do
      expect(page).to have_content product.name
    end
  end

  describe "screen transition" do
    scenario "user click site logo" do
      find('.navbar-brand').click
      expect(current_path).to eq potepan_index_path
    end

    scenario "user click product link" do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end
end
