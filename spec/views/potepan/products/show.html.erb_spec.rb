require 'rails_helper'

RSpec.describe "products/show.html.erb", type: :view do
  before do
    product.images << image
    visit potepan_product_path(product.id)
  end

  let(:taxon) { create(:taxon) }
  let(:image) { create(:image) }
  let(:product) { create(:product, taxons: [taxon]) }

  scenario "user click site logo" do
    find('.navbar-brand').click
    expect(current_path).to eq potepan_index_path
  end

  scenario "user click header right home link" do
    find('ul li a', text: 'Home').click
    expect(current_path).to eq potepan_index_path
  end

  scenario "user click light section home link" do
    find('ol li a', text: 'Home').click
    expect(current_path).to eq potepan_index_path
  end

  scenario "user click category link" do
    click_link '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
