require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :system do
  before do
    product1.images << image1
    product2.images << image2
    product_other.images << image_other
    visit potepan_category_path(taxon_leaf.id)
  end

  let(:taxonomy) { create(:taxonomy, name: "test-taxonomy") }
  let(:taxon) { create(:taxon, name: "test-taxon", taxonomy: taxonomy) }
  let(:taxon_leaf) { create(:taxon, name: "test-taxon", parent: taxon) }
  let(:product1) { create(:product, taxons: [taxon_leaf], name: "test-product1") }
  let(:product2) { create(:product, taxons: [taxon_leaf], name: "test-product2") }
  let(:taxon_other) { create(:taxon, name: "test-taxon-other") }
  let(:product_other) { create(:product, taxons: [taxon_other], name: "test-product-other") }
  let(:image1) { create(:image) }
  let(:image2) { create(:image) }
  let(:image_other) { create(:image) }

  describe "sidebar function" do
    scenario "sidebar has taxon and taxonomy name" do
      within '.side-nav' do
        expect(page).to have_content taxonomy.name
        expect(page).to have_content taxon_leaf.name
      end
    end

    scenario "user click taxon name" do
      within '.side-nav' do
        click_link taxon_leaf.name
        expect(current_path).to eq potepan_category_path(taxon_leaf.id)
      end
    end

    scenario "show only related products" do
      expect(page).to have_content 'test-product1'
      expect(page).to have_content 'test-product2'
      expect(page).not_to have_content 'test-product-other'
    end
  end
end
