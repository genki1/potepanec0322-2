require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  describe "GET /show" do
    before do
      product.images << image
      get potepan_category_path(taxon.id)
    end

    let(:image) { create(:image) }
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }

    it "returns http success" do
      expect(response).to have_http_status(:success)
    end

    it "returns taxon info" do
      expect(response.body).to include taxon.name
    end

    it "returns product info" do
      expect(response.body).to include product.name
      expect(response.body).to include product.price.to_s
      expect(response.body).to include product.display_image.attachment(:product)
    end
  end
end
