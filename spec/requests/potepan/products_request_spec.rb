require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }

  describe "GET /show" do
    it "returns http success" do
      get potepan_product_path(product.id)
      expect(response).to have_http_status(:success)
      expect(response.body).to include product.name
      expect(response.body).to include product.price.to_s
      expect(response.body).to include product.description
    end
  end
end
