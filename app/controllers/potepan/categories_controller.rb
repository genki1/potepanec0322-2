class Potepan::CategoriesController < ApplicationController
  def show
    @taxons = Spree::Taxon.all.includes(:products)
    @current_taxon = Spree::Taxon.find_by(id: params[:id])
    @products = @current_taxon.all_products.includes(master: [:images])
  end
end
